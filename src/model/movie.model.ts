import {model, Schema} from 'mongoose';

interface IReward {
  nominations: number;
  text: string;
  wins: number;
}

interface IMovie {
  title: string;
  directors: string[];
  genre: string[];
  num_comments: number;
  awards: IReward;
  text: string;
}

const movieSchema = new Schema<IMovie>({
  title: {
    type: Schema.Types.String,
    required: true,
  },

  num_comments: {
    type: Schema.Types.Number,
    required: true,
    default: 0,
  },

  directors: {
    type: [Schema.Types.String],
    required: true,
    default: [],
  },

  genre: {
    type: [Schema.Types.String],
    required: true,
    default: [],
  },
  awards: {
    nominations: {
      type: Schema.Types.Number,
      required: true,
      default: 0,
    },
    wins: {
      type: Schema.Types.Number,
      required: true,
      default: 0,
    },
  },
});

movieSchema.virtual('text').get(function (this: IMovie) {
  return `wins: ${this.awards.wins}`;
});

const Movie = model<IMovie>('Movie', movieSchema, 'movies');

export default Movie;
