import mongoose from 'mongoose';
import config from 'config';
import userModel from './model/user';
import Movie from './model/movie.model';

const MONGO_URL = config.get<string>('mongo.host');
const MONGO_USERNAME = config.get<string>('mongo.username');
const MONGO_PASSWORD = config.get<string>('mongo.password');
const MONGO_DATABASE = config.get<string>('mongo.database');
const MONGO_CONNECTION_TIMEOUT_MS = config.get<number>('mongo.timeoutMs');
const connectToMongo = async () => {
  try {
    const client = await mongoose.connect(MONGO_URL, {
      user: MONGO_USERNAME,
      pass: MONGO_PASSWORD,
      dbName: MONGO_DATABASE,
      connectTimeoutMS: MONGO_CONNECTION_TIMEOUT_MS,
    });

    const connectionInfo = client.connection;
    console.log(
      `[MONGO] Connection Success to host ${connectionInfo?.host} and database ${connectionInfo?.db.databaseName}`
    );
  } catch (error) {
    console.log(`[MONGO] Connection Fail: ${error}`);
    return false;
  }
  return true;
};

const findWithLimit = async () => {
  const users = await userModel.find().limit(3);
  console.log('find with limit 3', users);
};

const findMoviesByLean = async () => {
  const movieLean = await Movie.findById('573a1390f29313caabcd4135').lean();
  const movie = await Movie.findById('573a1390f29313caabcd4135');

  console.log(`Text in Movie with .lean() is ${movieLean?.text}`);
  console.log(`Text in Movie without .lean() is ${movie?.text}`);
};

const main = async () => {
  const isConnectionSuccess = await connectToMongo();

  if (!isConnectionSuccess) {
    return;
  }

  await findMoviesByLean();
};

main();
